-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           10.4.25-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour luna_maya_db
CREATE DATABASE IF NOT EXISTS `luna_maya_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `luna_maya_db`;

-- Listage de la structure de table luna_maya_db. accommodation
CREATE TABLE IF NOT EXISTS `accommodation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accom_arrival_date` date DEFAULT NULL,
  `accom_city` varchar(255) DEFAULT NULL,
  `accom_country` varchar(255) DEFAULT NULL,
  `accom_depart_date` date DEFAULT NULL,
  `accom_description` varchar(255) DEFAULT NULL,
  `accom_disponibility` bit(1) DEFAULT NULL,
  `accom_name` varchar(255) DEFAULT NULL,
  `accom_photo` varchar(255) DEFAULT NULL,
  `accom_price` int(11) DEFAULT NULL,
  `accom_quantity` int(11) DEFAULT NULL,
  `accom_room_type` varchar(255) DEFAULT NULL,
  `accom_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.accommodation : ~6 rows (environ)
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (1, NULL, 'Poitiers', 'France', NULL, 'Une super vue depuis les hauteurs. dormez comme un oiseau.', b'1', 'Les arbres perchés', 'resources/pictures/accommodation/cabane_triangle.jpg', 60, NULL, 'chambre double', 'Cabane');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (2, NULL, 'Venezia', 'Italie', NULL, 'Hotel cosy et friendly pour profiter en couple.', b'1', 'Hotel La Lune', 'resources/pictures/accommodation/hotel_dreamcatcher.jpg', 80, NULL, 'chambre double', 'Hotel');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (4, NULL, 'Nantes', 'France', NULL, 'Petit hotel tres sympa au coeur de la ville.', b'1', 'Hotel Les moulins', 'resources/pictures/accommodation/hotel_portree.jpg', 45, NULL, 'chambre triple', 'Hotel');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (21, NULL, 'Tulum', 'Mexique', NULL, 'Ton reveil avec la vue la plus turquoise pour te donner du peps', b'1', 'Cabanes les Palmes', 'resources/pictures/accommodation/cabane_palmes.jpg', 70, NULL, 'Chambre double avec vue a l''ocean', 'Cabane');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (26, NULL, 'Gotemborg', 'Suède', NULL, 'Dormez en plein fôret dans las plus belle cabane sphere.', b'1', 'Cabane Sphere', 'resources/pictures/accommodation/cabane_sphere.jpg', 70, NULL, '5 personnes', 'Cabane');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (31, NULL, 'Dhigu', 'Maldives', NULL, 'Vivez l''éxperience d''une cabane sur pilotis avec une vue incroyable entouré de la mer.', b'1', 'Hotel Turquoise', 'resources/pictures/accommodation/hotel_turquoise.jpg', 100, NULL, 'Cabane pour 4 personnes', 'Cabane');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (32, NULL, 'Paris', 'France', NULL, 'Petit apartement confortable à Paris.', b'1', 'Appartement Cosy', 'resources/pictures/accommodation/location_appartCosy.jpg', 80, NULL, 'Chambre double', 'Location');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (33, NULL, 'Londres', 'Angleterre', NULL, 'Partagez entre amis de ce petit hotel sympa trés bien placé.', b'1', 'Hotel Londres', 'resources/pictures/accommodation/hotel_london.jpg', 80, NULL, 'Chambre double', 'Hotel');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (34, NULL, 'Vallon Pont D''arc', 'France', NULL, 'Partez en Ardeche pour decouvrir le Vallon Pont Darc et dormez dans ce formidable Tipie', b'1', 'Camping les Tipies', 'resources/pictures/accommodation/camping_tipieGrand.jpg', 40, NULL, 'Toile de tente', 'Camping');
INSERT INTO `accommodation` (`id`, `accom_arrival_date`, `accom_city`, `accom_country`, `accom_depart_date`, `accom_description`, `accom_disponibility`, `accom_name`, `accom_photo`, `accom_price`, `accom_quantity`, `accom_room_type`, `accom_type`) VALUES (35, NULL, 'Touars', 'France', NULL, 'Vivez une grand expérience dans cet agréable Tipie', b'1', 'Les grands Tipies', 'resources/pictures/accommodation/camping_tipies.jpg', 30, NULL, 'Tipie entier', 'Camping');

-- Listage de la structure de table luna_maya_db. activity
CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_city` varchar(255) DEFAULT NULL,
  `activity_country` varchar(255) DEFAULT NULL,
  `activity_date` date DEFAULT NULL,
  `activity_description` varchar(255) DEFAULT NULL,
  `activity_disponibility` bit(1) DEFAULT NULL,
  `activity_name` varchar(255) DEFAULT NULL,
  `activity_photo` varchar(255) DEFAULT NULL,
  `activity_price` int(11) DEFAULT NULL,
  `activity_quantity` int(11) DEFAULT NULL,
  `activity_time` date DEFAULT NULL,
  `activity_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.activity : ~0 rows (environ)

-- Listage de la structure de table luna_maya_db. blog
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_city` varchar(255) DEFAULT NULL,
  `article_country` varchar(255) DEFAULT NULL,
  `article_description` varchar(255) DEFAULT NULL,
  `article_name` varchar(255) DEFAULT NULL,
  `article_photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.blog : ~0 rows (environ)

-- Listage de la structure de table luna_maya_db. booking
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_date` date DEFAULT NULL,
  `insurance` bit(1) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `a_pack_id` int(11) DEFAULT NULL,
  `accommodation_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `transport_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3omyrcnfpmeu34vxcv1pm83vp` (`a_pack_id`),
  KEY `FK5uxucbfmlrnnjunuxoei5ux0s` (`accommodation_id`),
  KEY `FKn3p0dqhv1p3hl1gv6a2ltvsbu` (`activity_id`),
  KEY `FK70t92vvx289ayx2hq2v4hdcjl` (`payment_id`),
  KEY `FKct5ry25xkobf1ylgtiqti42ss` (`transport_id`),
  KEY `FKkgseyy7t56x7lkjgu3wah5s3t` (`user_id`),
  CONSTRAINT `FK3omyrcnfpmeu34vxcv1pm83vp` FOREIGN KEY (`a_pack_id`) REFERENCES `pack` (`id`),
  CONSTRAINT `FK5uxucbfmlrnnjunuxoei5ux0s` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation` (`id`),
  CONSTRAINT `FK70t92vvx289ayx2hq2v4hdcjl` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`),
  CONSTRAINT `FKct5ry25xkobf1ylgtiqti42ss` FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`),
  CONSTRAINT `FKkgseyy7t56x7lkjgu3wah5s3t` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKn3p0dqhv1p3hl1gv6a2ltvsbu` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=379 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.booking : ~66 rows (environ)
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES (1, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(247, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(248, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(251, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(252, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(253, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(255, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(256, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(257, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 5);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(258, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(259, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, 7);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(260, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(261, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(262, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(265, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(267, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(268, NULL, NULL, NULL, NULL, 26, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(269, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(270, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(273, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(274, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(276, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 10);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(277, NULL, NULL, NULL, NULL, 21, NULL, NULL, NULL, 10);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(278, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(279, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(280, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(282, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(284, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(285, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(286, NULL, NULL, NULL, NULL, 21, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(287, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(288, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(289, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(290, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(291, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(292, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(294, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(295, '2023-04-28', NULL, NULL, NULL, 26, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(297, '2023-04-28', NULL, NULL, NULL, 21, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(298, '2023-04-28', NULL, NULL, NULL, 4, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(299, '2023-07-14', NULL, NULL, NULL, 26, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(300, '2023-07-14', NULL, NULL, NULL, 4, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(302, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(311, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(312, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(313, NULL, NULL, NULL, NULL, 21, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(314, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(315, '2023-04-09', NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(316, '1970-01-01', NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(317, '1970-01-01', NULL, NULL, NULL, 4, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(318, '2023-04-09', NULL, NULL, NULL, 2, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(319, '2023-04-09', NULL, NULL, NULL, 2, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(320, '2023-04-09', NULL, NULL, NULL, 2, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(326, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(327, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(328, '2023-04-09', NULL, NULL, NULL, 4, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(329, '2023-04-09', NULL, NULL, NULL, 26, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(330, '3923-06-06', NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(331, '1923-06-06', NULL, NULL, NULL, 4, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(332, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(333, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(334, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(335, NULL, NULL, NULL, NULL, 21, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(336, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(337, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(338, '2023-05-27', NULL, NULL, NULL, 31, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(339, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(340, '2023-06-22', NULL, NULL, NULL, 31, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(342, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(343, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(344, '2023-04-21', NULL, NULL, NULL, 31, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(345, NULL, NULL, NULL, NULL, 21, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(346, '2023-05-25', NULL, NULL, NULL, 32, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(347, NULL, NULL, NULL, NULL, 31, NULL, NULL, NULL, 9);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(348, NULL, NULL, NULL, NULL, 31, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(349, '2023-07-28', NULL, NULL, NULL, 32, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(351, '2023-05-25', NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(352, NULL, NULL, NULL, NULL, 26, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(353, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(363, '2023-07-20', NULL, NULL, NULL, 32, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(364, '2023-07-20', NULL, NULL, NULL, 1, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(377, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 4);
INSERT INTO `booking` (`id`, `booking_date`, `insurance`, `status`, `a_pack_id`, `accommodation_id`, `activity_id`, `payment_id`, `transport_id`, `user_id`) VALUES	(378, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, 4);

-- Listage de la structure de table luna_maya_db. comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_date` date DEFAULT NULL,
  `user_message` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqi14bvepnwtjbbaxm7m4v44yg` (`user_id`),
  CONSTRAINT `FKqi14bvepnwtjbbaxm7m4v44yg` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.comments : ~0 rows (environ)

-- Listage de la structure de table luna_maya_db. pack
CREATE TABLE IF NOT EXISTS `pack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pack_accomodation` varchar(255) DEFAULT NULL,
  `pack_activity` varchar(255) DEFAULT NULL,
  `pack_depart_date` date DEFAULT NULL,
  `pack_description` varchar(255) DEFAULT NULL,
  `pack_disponibility` bit(1) DEFAULT NULL,
  `pack_name` varchar(255) DEFAULT NULL,
  `pack_photo` varchar(255) DEFAULT NULL,
  `pack_price` int(11) DEFAULT NULL,
  `pack_quantity` int(11) DEFAULT NULL,
  `pack_return_date` date DEFAULT NULL,
  `pack_transport` varchar(255) DEFAULT NULL,
  `blog_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfu8gm460ea6rjllsa8awhmdpi` (`blog_id`),
  CONSTRAINT `FKfu8gm460ea6rjllsa8awhmdpi` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.pack : ~7 rows (environ)
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (1, 'Cabane au bord de l''eau', 'Randonné et dégustation de fromages de montagne', '2023-04-28' ,'Prendre de l''air frais aux Alpes français.', b'1', 'Lac secret', 'resources/pictures/pack/lune.jpg', 150, NULL, '2023-04-30', 'Bus aller retour', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (2, 'Camping au bord de l''eau', '2 journées de Kayak', '2023-05-18', 'Voyage à la nature du Lac Braies au nord de l''Italie', b'1', 'Laco Braies', 'resources/pictures/pack/italiaLacoBraies.jpg', 300, NULL, '2023-05-21', 'Train compris aller retour', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (3, 'Hotel Miguel Angel, en chambre double.', 'Visite guidée du centre ville', '2023-05-26', 'Passez un excelent week-end à Venezia et profitez de sa magie !', b'1', 'Week-end Venezia', 'resources/pictures/pack/venezia.jpg', 250, NULL, '2023-05-28', 'Train aller retour Paris Venezia. Départ 8:00 am', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (4, 'Hotel Costar en chambre double.', 'Dégustation de bières', '2023-05-17', 'Partez en Allemagne pour découvrir la belle ville de Rothenburg !', b'1', 'Rothenburg', 'resources/pictures/pack/germanyRothenburg.jpg', 350, NULL, '2023-05-22', 'Train aller-retour, départ 9:00 am', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (5, 'Cabane au bord de la plage pour 4 personnes', 'Snorkeling pour voir les tortues', '2023-06-01', 'Partez au soleil du Caraibe. Voyage a Tulum, Mexique', b'1', 'Tulum magique', 'resources/pictures/pack/mexique.jpg', 1500, NULL, '2023-06-09', 'Vols aller-retour', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (6, 'Hotel écologique a 1 km de la plage. Chambre double', 'Randonée pour découvrir les falaises.', '2023-06-10', 'Decouvrez Bali et sa plage de Nusa Penida.', b'1', 'Bali Secret', 'resources/pictures/pack/baliNusaPenida.jpg', 2400, NULL, '2023-06-18', 'Vols aller-retour', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (7, 'Hotel Bohemian en chambre double', 'Tour de découverte de la ville.', '2023-06-22', 'Découvrez Sintra et sa plage de Ursa', b'1', 'Plage portugaise', 'resources/pictures/pack/portugalSintraUrsaBeach.jpg', 400, NULL, '2023-06-26', 'Vols aller-retour', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (9, 'Dormez dans une fabrique de Sirop d''érable', 'Decouverte de National Part et du Capilano Bridge', '2023-05-15', 'Partez dans la nature de Vancouver', b'1', 'Decouvrez Vancouver', 'resources/pictures/pack/canadaCapilanoBridge.jpg', 350, NULL, '2023-06-15', 'avion compris aller retour Paris-Vancouver', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (10, 'Hotel Los Soles au centre ville ', 'Degustation de café local, Exploration de la jungle Ladandona en randonné', '2023-06-05', 'Partez decouvrir un village Magique au coeur du Chiapas, Mexique', b'1', 'San Cristobal', 'resources/pictures/pack/sanCristobal.jpg', 1500, NULL, '2023-06-15', 'Vols Aller-Retour Paris-Mexico et Mexico-Tuxtla', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (12, 'Hotel compris pour 2 personnes', 'Randonné guidé a traver le Canyon', '2023-06-08', 'Partez a la decouverte du Gran Canyon du Colorado aux Etats Unis', b'1', 'Colorado bien rouge', 'resources/pictures/pack/colorado.jpg', 1200, NULL, '2023-06-18', 'Vol aller -retour France-USA', NULL);
INSERT INTO `pack` (`id`, `pack_accomodation`, `pack_activity`, `pack_depart_date`, `pack_description`, `pack_disponibility`, `pack_name`, `pack_photo`, `pack_price`, `pack_quantity`, `pack_return_date`, `pack_transport`, `blog_id`) VALUES (13, 'Camping a la belle étoile pour profiter de lumières', '3 nuits d''atelier de photo nocturne', '2023-05-25', 'Vivez l''expérience magique de lumières polaires', b'1', 'Aurores Boréales', 'resources/pictures/pack/northernLightGreen.jpg', 600, NULL, '2023-05-31', 'Vols aller retour France-Norvège', NULL);

-- Listage de la structure de table luna_maya_db. payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `payment_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.payment : ~0 rows (environ)

-- Listage de la structure de table luna_maya_db. role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.role : ~2 rows (environ)
INSERT INTO `role` (`id`, `name`) VALUES (1, 'CLIENT');
INSERT INTO `role` (`id`, `name`) VALUES (2, 'ADMIN');

-- Listage de la structure de table luna_maya_db. transport
CREATE TABLE IF NOT EXISTS `transport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_city` varchar(255) DEFAULT NULL,
  `from_country` varchar(255) DEFAULT NULL,
  `to_city` varchar(255) DEFAULT NULL,
  `to_country` varchar(255) DEFAULT NULL,
  `transp_dept_date` date DEFAULT NULL,
  `transp_dept_time` date DEFAULT NULL,
  `transp_disponibility` bit(1) DEFAULT NULL,
  `transp_return_date` date DEFAULT NULL,
  `transp_return_time` date DEFAULT NULL,
  `transport_name` varchar(255) DEFAULT NULL,
  `transport_price` int(11) DEFAULT NULL,
  `transport_quantity` int(11) DEFAULT NULL,
  `transport_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.transport : ~0 rows (environ)

-- Listage de la structure de table luna_maya_db. user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ew1hvam8uwaknuaellwhqchhb` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.user : ~9 rows (environ)
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (1, 'dali@gmail.com', 'daliLogin', 'Dali ', '$2a$10$d7EnSUn4pOdVfIESdW6qgeOd8hfpq/fWGMBe8KbPPus2ULSQ672my', '0778796278', 'Logbo');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (2, 'guede@gmail.com', 'guedeLogin', 'Guede', '$2a$10$6rxceb6TPDMsK5DW8KZn.eYNCCKKSTtzVETCAEGow678Cq1fbuwj2', '5255123469', 'Logbo');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (3, 'noemi@gmail.com', 'noemiLogin', 'Noemi', '$2a$10$Rq8tvhnZdTC9r.Umm1dl0esh4ZEGxAFNGUHpXIZJulFfDANcN7edq', '55737779', 'Alfaro');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (4, 'sophie@yahoo.com', 'sophieLogin', 'Sophie', '$2a$10$bgxdqty6Q9oVYs66EuIwKO41.EOsyvCVEFSdfUNV/6jM6B11T0ai.', '5263987410', 'Rodriguez');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (5, 'ixchel@gmail.com', 'ixchelLogin', 'Ixchel', '$2a$10$OJbtkOSKF0qczCVKrIefVOphXZBm9fi6BvuPL2HAA7CbpKnwyzBZ.', '078965231475', 'Alfaro');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (7, 'nancy@gmail.com', 'nancyLogin', 'Nancy', '$2a$10$nNJsKryyHCELDAelSiE3R.1YqaXxA9PqqKn8wxO5HSHgm3aNFTiY6', '5623147895', 'Garcia');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (8, 'myriam@yahoo.com', 'myriamLogin', 'Myriam', '$2a$10$nMrO5HQKWBuaWJUuXhk1C.YgOqtNVPtRmCzra17thF5BbH1gsYdcS', '8963214578', 'Sauterrau');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (9, 'elias@gmail.com', 'eliasLogin', 'Elias', '$2a$10$CM5/Sdpx2rEX3aCPBWRt.eG79nBZXCFnGJhPq8yRWFR1lw.7H2Y1K', '8963214578', 'Pittman');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (10, 'florent@mail.com', 'florentLogin', 'Florent', '$2a$10$am9Zycghn8m0MRnKYjUXOuDmZYeoUswI7/suKa6O.rv.m7zJwxKCa', '8569321478', 'Chedreau');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (12, 'test@test.com', 'PrenomLogin', 'PrenomTest', '$2a$10$RxEwtQI7Z/R0gvdwfwrxeOYjD/3pjJfuLDs1EcYIYZFqMEIoJq3tm', '7852368596', 'NomTest');
INSERT INTO `user` (`id`, `address`, `login`, `name`, `password`, `phone`, `surname`) VALUES (15, 'admin@mail.com', 'AdminLogin', 'AdminPrenom', '$2a$10$SCJ0nFGERScuL1Fksg2RDeHdi4u5O8qZpYLml.s6bQ3SsjVVjYa22', '7896524132', 'AdminNom');

-- Listage de la structure de table luna_maya_db. user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  KEY `FKj9553ass9uctjrmh0gkqsmv0d` (`roles_id`),
  KEY `FK55itppkw3i07do3h7qoclqd4k` (`user_id`),
  CONSTRAINT `FK55itppkw3i07do3h7qoclqd4k` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKj9553ass9uctjrmh0gkqsmv0d` FOREIGN KEY (`roles_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table luna_maya_db.user_roles : ~9 rows (environ)
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES (1, 2);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(2, 1);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(3, 2);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(4, 1);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(5, 1);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(7, 2);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(8, 2);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(9, 1);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(10, 1);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(12, 1);
INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES	(15, 2);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
