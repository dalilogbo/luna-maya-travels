package fr.eql.ai113.lmtravels.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Date;

/**
 *
 */
@Entity
public class Comments {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String user_message;
    public Date comment_date;

    /**
     * Association with 'User' table
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User user;

    /**
     * Default constructor
     */
    public Comments() {
    }

    /**
     * Complete Comments constructor
     */
    public Comments(String user_message, Date comment_date, User user) {
        this.user_message = user_message;
        this.comment_date = comment_date;
        this.user = user;
    }

    /**
     * This method allows to Create a comment.
     */
    public void addComment() {
        // TODO implement here
    }

    /**
     * This method allows to Search a comment.
     */
    public void searchComment() {
        // TODO implement here
    }

    //Getters//

    public Integer getId() {
        return id;
    }

    public String getUser_message() {
        return user_message;
    }

    public Date getComment_date() {
        return comment_date;
    }

    public User getUser() {
        return user;
    }


    //Setters//

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUser_message(String user_message) {
        this.user_message = user_message;
    }

    public void setComment_date(Date comment_date) {
        this.comment_date = comment_date;
    }

    public void setUser(User user) {
        this.user = user;
    }
}