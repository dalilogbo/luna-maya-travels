package fr.eql.ai113.lmtravels.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 *
 */
@Entity
public class Booking {

    /**
     * Default constructor
     */
    public Booking() {
    }

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public Date booking_date;
    public Boolean insurance;
    public String status;

    /**
     * Association with 'Payment' table
     */
    @OneToOne(targetEntity = Payment.class, cascade = CascadeType.ALL)
    private Payment payment;

    /**
     * Association with 'User' table
     */
//    @JsonIgnore
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User user;

    /**
     * Association with 'Activity' table
     */
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Activity activity;

    /**
     * Association with 'Pack' table
     */
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Pack aPack;


    /**
     * Association with 'Transport' table
     */
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Transport transport;

    /**
     * Association with 'Accommodation' table
     */
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Accommodation accommodation;


    /**
     * Complete Booking constructor
     */
    public Booking(Integer id, Date booking_date, Boolean insurance, String status, Payment payment, User user, Activity activity, Pack aPack, Transport transport, Accommodation accommodation) {
        this.id = id;
        this.booking_date = booking_date;
        this.insurance = insurance;
        this.status = status;
        this.payment = payment;
        this.user = user;
        this.activity = activity;
        this.aPack = aPack;
        this.transport = transport;
        this.accommodation = accommodation;
    }

    public Booking(User user, Pack aPack) {
        this.user = user;
        this.aPack = aPack;
    }

    public Booking(User user, Accommodation accommodation) {
        this.user = user;
        this.accommodation = accommodation;
    }



    //Getters//

    public Integer getId() {
        return id;
    }
    public Date getBooking_date() {
        return booking_date;
    }
    public Boolean isInsurance() {
        return insurance;
    }
    public String getStatus() {
        return status;
    }
    public Payment getPayment() {
        return payment;
    }
    public User getUser() {
        return user;
    }
    public Activity getActivity() {
        return activity;
    }
    public Pack getaPack() {
        return aPack;
    }
    public Transport getTransport() {
        return transport;
    }

    public Accommodation getAccommodation() {
        return accommodation;
    }
    //Setters//

    public void setId(Integer id) {
        this.id = id;
    }
    public void setBooking_date(Date booking_date) {
        this.booking_date = booking_date;
    }
    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setPayment(Payment payment) {
        this.payment = payment;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void setActivity(Activity activity) {
        this.activity = activity;
    }
    public void setaPack(Pack aPack) {
        this.aPack = aPack;
    }
    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }
}
