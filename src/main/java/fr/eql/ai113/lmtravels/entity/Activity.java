package fr.eql.ai113.lmtravels.entity;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

/**
 *
 */
@Entity
public class Activity implements Serializable {

    /**
     * Default constructor
     */
    public Activity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String activity_type;
    public String activity_name;
    public String activity_country;
    public String activity_city;
    public  String activity_description;
    public  String activity_photo;
    public Boolean activity_disponibility;
    public Date activity_date;
    public Date activity_time;
    public Integer activity_quantity;
    public Integer activity_price;

    /**
     * Association with 'Booking' table
     */
    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;


    /**
     * Complete Activity constructor
     */
    public Activity(Integer id, String activity_type, String activity_name, String activity_country, String activity_city, String activity_description, String activity_photo, Boolean activity_disponibility, Date activity_date, Date activity_time, Integer activity_quantity, Integer activity_price, List<Booking> bookings) {
        this.id = id;
        this.activity_type = activity_type;
        this.activity_name = activity_name;
        this.activity_country = activity_country;
        this.activity_city = activity_city;
        this.activity_description = activity_description;
        this.activity_photo = activity_photo;
        this.activity_disponibility = activity_disponibility;
        this.activity_date = activity_date;
        this.activity_time = activity_time;
        this.activity_quantity = activity_quantity;
        this.activity_price = activity_price;
        this.bookings = bookings;
    }

    /**
     * This method allows to Create an Activity
     */
    public void addActivity() {
        // TODO implement here
    }

    /**
     * This method allows to Search an Activity
     */
    public void searchActivity() {
        // TODO implement here
    }

    /**
     * This method allows to Modify Activity's information
     */
    public void editActivity() {
        // TODO implement here
    }

    /**
     * This method allows to Delete an Activity
     */
    public void deleteActivity() {
        // TODO implement here
    }

    //Getters//

    public Integer getId() {
        return id;
    }

    public String getActivity_type() {
        return activity_type;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public String getActivity_country() {
        return activity_country;
    }

    public String getActivity_city() {
        return activity_city;
    }

    public String getActivity_description() {
        return activity_description;
    }

    public String getActivity_photo() {
        return activity_photo;
    }

    public Boolean getActivity_disponibility() {
        return activity_disponibility;
    }

    public Date getActivity_date() {
        return activity_date;
    }

    public Date getActivity_time() {
        return activity_time;
    }

    public Integer getActivity_quantity() {
        return activity_quantity;
    }

    public Integer getActivity_price() {
        return activity_price;
    }

    public List<Booking> getBookings() {
        return bookings;
    }


    //Setters//


    public void setId(Integer id) {
        this.id = id;
    }

    public void setActivity_type(String activity_type) {
        this.activity_type = activity_type;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public void setActivity_country(String activity_country) {
        this.activity_country = activity_country;
    }

    public void setActivity_city(String activity_city) {
        this.activity_city = activity_city;
    }

    public void setActivity_description(String activity_description) {
        this.activity_description = activity_description;
    }

    public void setActivity_photo(String activity_photo) {
        this.activity_photo = activity_photo;
    }

    public void setActivity_disponibility(Boolean activity_disponibility) {
        this.activity_disponibility = activity_disponibility;
    }

    public void setActivity_date(Date activity_date) {
        this.activity_date = activity_date;
    }

    public void setActivity_time(Date activity_time) {
        this.activity_time = activity_time;
    }

    public void setActivity_quantity(Integer activity_quantity) {
        this.activity_quantity = activity_quantity;
    }

    public void setActivity_price(Integer activity_price) {
        this.activity_price = activity_price;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}