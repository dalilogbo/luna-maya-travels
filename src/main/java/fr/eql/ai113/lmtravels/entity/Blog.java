package fr.eql.ai113.lmtravels.entity;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 *
 */
@Entity
public class Blog {

    /**
     * Default constructor
     */

    public Blog() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String article_name;
    public String article_description;
    public String article_country;
    public String article_city;
    public String article_photo;

    /**
     * Association with 'Pack' table
     */

    @OneToMany(mappedBy = "blog", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Pack> packs;

    /**
     * Complete Blog constructor
     */
    public Blog(Integer id, String article_name, String article_description, String article_country, String article_city, String article_photo, List<Pack> packs) {
        this.id = id;
        this.article_name = article_name;
        this.article_description = article_description;
        this.article_country = article_country;
        this.article_city = article_city;
        this.article_photo = article_photo;
        this.packs = packs;
    }

    /**
     * This method allows to Create a new editorial Article
     */
    public void addArticle() {
        // TODO implement here
    }

    /**
     * This method allows to Search an editorial Article
     */
    public void searchArticle() {
        // TODO implement here
    }

    /**
     * This method allows to Modify an editorial Article information
     */
    public void editArticle() {
        // TODO implement here
    }

    /**
     * This method allows to Delete an editorial Article
     */
    public void deleteArticle() {
        // TODO implement here
    }

    //Getters//

    public Integer getId() {
        return id;
    }

    public String getArticle_name() {
        return article_name;
    }

    public String getArticle_description() {
        return article_description;
    }

    public String getArticle_country() {
        return article_country;
    }

    public String getArticle_city() {
        return article_city;
    }

    public String getArticle_photo() {
        return article_photo;
    }

    public List<Pack> getPacks() {
        return packs;
    }


    //Setters//


    public void setId(Integer id) {
        this.id = id;
    }

    public void setArticle_name(String article_name) {
        this.article_name = article_name;
    }

    public void setArticle_description(String article_description) {
        this.article_description = article_description;
    }

    public void setArticle_country(String article_country) {
        this.article_country = article_country;
    }

    public void setArticle_city(String article_city) {
        this.article_city = article_city;
    }

    public void setArticle_photo(String article_photo) {
        this.article_photo = article_photo;
    }

    public void setPacks(List<Pack> packs) {
        this.packs = packs;
    }
}