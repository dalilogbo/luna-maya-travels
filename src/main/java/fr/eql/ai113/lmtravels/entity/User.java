package fr.eql.ai113.lmtravels.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles;
    public String name;
    public String surname;
//    @JsonIgnore
    @Column(unique = true)
    public String login;
    @JsonIgnore
    public String password;
    public String phone;
    public String address;
    /**
     * Association with 'Comments' table
     */
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Comments> comments = new ArrayList<>();
    /**
     * Association with 'Booking' table
     */
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }
    @Override
    public String getUsername() {
        return login;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }

    ///Constructors///

    public User() {
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User( String name, String surname, String login, String password, String phone, String address) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    public User(Collection<Role> roles, String name, String surname, String login, String password, String phone, String address) {
        this.roles = roles;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    //Getters//
    public Integer getId() {
        return id;
    }
    public Collection<Role> getRoles() {
        return roles;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public String getPhone() {
        return phone;
    }
    public String getAddress() {
        return address;
    }
    public List<Comments> getComments() {
        return comments;
    }
    public List<Booking> getBookings() {
        return bookings;
    }



    //Setters//
    public void setId(Integer id) {
        this.id = id;
    }
    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }
    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }


}