package fr.eql.ai113.lmtravels.entity.dto;

import java.sql.Date;

public class AccommodationDto {

    public Integer id;
    public  String accom_type;
    public  String accom_name;
    public  String accom_country;
    public  String accom_city;
    public  String accom_description;
    public  String accom_room_type;
    public  String accom_photo;
    public Boolean accom_disponibility;
    public Date accom_arrival_date;
    public  Date accom_depart_date;
    public Integer accom_quantity;
    public Integer accom_price;

    ///Getters///
    public Integer getId() {
        return id;
    }
    public String getAccom_type() {
        return accom_type;
    }
    public String getAccom_name() {
        return accom_name;
    }
    public String getAccom_country() {
        return accom_country;
    }
    public String getAccom_city() {
        return accom_city;
    }
    public String getAccom_description() {
        return accom_description;
    }
    public String getAccom_room_type() {
        return accom_room_type;
    }
    public String getAccom_photo() {
        return accom_photo;
    }
    public Boolean getAccom_disponibility() {
        return accom_disponibility;
    }
    public Date getAccom_arrival_date() {
        return accom_arrival_date;
    }
    public Date getAccom_depart_date() {
        return accom_depart_date;
    }
    public Integer getAccom_quantity() {
        return accom_quantity;
    }
    public Integer getAccom_price() {
        return accom_price;
    }


    ///Setters for Tests///
    public void setId(Integer id) {
        this.id = id;
    }
    public void setAccom_type(String accom_type) {
        this.accom_type = accom_type;
    }
    public void setAccom_name(String accom_name) {
        this.accom_name = accom_name;
    }
    public void setAccom_country(String accom_country) {
        this.accom_country = accom_country;
    }
    public void setAccom_city(String accom_city) {
        this.accom_city = accom_city;
    }
    public void setAccom_description(String accom_description) {
        this.accom_description = accom_description;
    }
    public void setAccom_room_type(String accom_room_type) {
        this.accom_room_type = accom_room_type;
    }
    public void setAccom_photo(String accom_photo) {
        this.accom_photo = accom_photo;
    }
    public void setAccom_disponibility(Boolean accom_disponibility) {
        this.accom_disponibility = accom_disponibility;
    }
    public void setAccom_arrival_date(Date accom_arrival_date) {
        this.accom_arrival_date = accom_arrival_date;
    }
    public void setAccom_depart_date(Date accom_depart_date) {
        this.accom_depart_date = accom_depart_date;
    }
    public void setAccom_quantity(Integer accom_quantity) {
        this.accom_quantity = accom_quantity;
    }
    public void setAccom_price(Integer accom_price) {
        this.accom_price = accom_price;
    }
}
