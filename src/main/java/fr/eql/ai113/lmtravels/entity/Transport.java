package fr.eql.ai113.lmtravels.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.sql.Date;
import java.util.List;

@Entity
public class Transport {

    /**
     * Default constructor
     */
    public Transport() {
    }

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String transport_type;
    public String transport_name;
    public String from_country;
    public String from_city;
    public String to_country;
    public String to_city;
    public Boolean transp_disponibility;
    public Date transp_dept_date;
    public Date transp_dept_time;
    public Date transp_return_date;
    public Date transp_return_time;
    public Integer transport_quantity;
    public Integer transport_price;

    /**
     * Association with 'Booking' table
     */
    @OneToMany(mappedBy = "transport", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;

    /**
     * Complete Transport constructor
     */
    public Transport(Integer id, String transport_type, String transport_name, String from_country, String from_city, String to_country, String to_city, Boolean transp_disponibility, Date transp_dept_date, Date transp_dept_time, Date transp_return_date, Date transp_return_time, Integer transport_quantity, Integer transport_price, List<Booking> bookings) {
        this.id = id;
        this.transport_type = transport_type;
        this.transport_name = transport_name;
        this.from_country = from_country;
        this.from_city = from_city;
        this.to_country = to_country;
        this.to_city = to_city;
        this.transp_disponibility = transp_disponibility;
        this.transp_dept_date = transp_dept_date;
        this.transp_dept_time = transp_dept_time;
        this.transp_return_date = transp_return_date;
        this.transp_return_time = transp_return_time;
        this.transport_quantity = transport_quantity;
        this.transport_price = transport_price;
        this.bookings = bookings;
    }

    /**
     * This methode allows to Creat a new Transport
     */
    public void addTransport() {
        // TODO implement here
    }

    /**
     *This methode allows to Search a Transport
     */
    public void searchTransport() {
        // TODO implement here
    }

    /**
     *This methode allows to Modify Transport's information
     */
    public void editTransport() {
        // TODO implement here
    }

    /**
     *This methode allows to Delete a Transport
     */
    public void deleteTransport() {
        // TODO implement here
    }

    //Getters//

    public Integer getId() {
        return id;
    }

    public String getTransport_type() {
        return transport_type;
    }

    public String getTransport_name() {
        return transport_name;
    }

    public String getFrom_country() {
        return from_country;
    }

    public String getFrom_city() {
        return from_city;
    }

    public String getTo_country() {
        return to_country;
    }

    public String getTo_city() {
        return to_city;
    }

    public Boolean getTransp_disponibility() {
        return transp_disponibility;
    }

    public Date getTransp_dept_date() {
        return transp_dept_date;
    }

    public Date getTransp_dept_time() {
        return transp_dept_time;
    }

    public Date getTransp_return_date() {
        return transp_return_date;
    }

    public Date getTransp_return_time() {
        return transp_return_time;
    }

    public Integer getTransport_quantity() {
        return transport_quantity;
    }

    public Integer getTransport_price() {
        return transport_price;
    }

    public List<Booking> getBookings() {
        return bookings;
    }


    //Setters//


    public void setId(Integer id) {
        this.id = id;
    }

    public void setTransport_type(String transport_type) {
        this.transport_type = transport_type;
    }

    public void setTransport_name(String transport_name) {
        this.transport_name = transport_name;
    }

    public void setFrom_country(String from_country) {
        this.from_country = from_country;
    }

    public void setFrom_city(String from_city) {
        this.from_city = from_city;
    }

    public void setTo_country(String to_country) {
        this.to_country = to_country;
    }

    public void setTo_city(String to_city) {
        this.to_city = to_city;
    }

    public void setTransp_disponibility(Boolean transp_disponibility) {
        this.transp_disponibility = transp_disponibility;
    }

    public void setTransp_dept_date(Date transp_dept_date) {
        this.transp_dept_date = transp_dept_date;
    }

    public void setTransp_dept_time(Date transp_dept_time) {
        this.transp_dept_time = transp_dept_time;
    }

    public void setTransp_return_date(Date transp_return_date) {
        this.transp_return_date = transp_return_date;
    }

    public void setTransp_return_time(Date transp_return_time) {
        this.transp_return_time = transp_return_time;
    }

    public void setTransport_quantity(Integer transport_quantity) {
        this.transport_quantity = transport_quantity;
    }

    public void setTransport_price(Integer transport_price) {
        this.transport_price = transport_price;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
