package fr.eql.ai113.lmtravels.entity.dto;

import fr.eql.ai113.lmtravels.entity.Role;

import java.util.Collection;

public class UserDto {

    public Integer id;
    private Collection<Role> roles ;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String phone;
    private String address;

    /// Getters ///


    public Integer getId() {
        return id;
    }

    public Collection<Role> getRoles() {
        return roles;
    }


    public Collection<Role> getRoles(Role role) {
        return roles;
    }

    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public String getPhone() {
        return phone;
    }
    public String getAddress() {
        return address;
    }

    /// Setters for Tests///
    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}
