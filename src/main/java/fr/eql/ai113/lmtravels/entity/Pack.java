package fr.eql.ai113.lmtravels.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Pack {

    /**
     * Default constructor
     */
    public Pack() {
    }

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String pack_name;
    public String pack_description;
    public String pack_photo;
    public Boolean pack_disponibility;
    public LocalDate pack_depart_date;
    public LocalDate pack_return_date;
    public Integer pack_quantity;
    public Integer pack_price;
    public String pack_accomodation;
    public String pack_transport;
    public String pack_activity;


    /**
     * Association with 'Blog' table
     */
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Blog blog;

    /**
     * Association with 'Booking' table
     */
    @JsonIgnore
    @OneToMany(mappedBy = "aPack", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;


    /**
     * Complete Package constructor
     */

    public Pack(Integer id, String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_quantity, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity, Blog blog, List<Booking> bookings) {
        this.id = id;
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_quantity = pack_quantity;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
        this.blog = blog;
        this.bookings = bookings;
    }


    /**
     * Constructor for PackDTO without ID
     */

    public Pack(String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity) {
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
    }


    /**
     * Constructor for PackDTO with ID
     */
    public Pack(Integer id, String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_quantity, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity) {
        this.id = id;
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_quantity = pack_quantity;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
    }

    //Getters//
    public Integer getId() {
        return id;
    }

    public String getPack_name() {
        return pack_name;
    }

    public String getPack_description() {
        return pack_description;
    }

    public String getPack_photo() {
        return pack_photo;
    }

    public Boolean getPack_disponibility() {
        return pack_disponibility;
    }

    public LocalDate getPack_depart_date() {
        return pack_depart_date;
    }

    public LocalDate getPack_return_date() {
        return pack_return_date;
    }

    public Integer getPack_quantity() {
        return pack_quantity;
    }

    public Integer getPack_price() {
        return pack_price;
    }

    public String getPack_accomodation() {
        return pack_accomodation;
    }

    public String getPack_transport() {
        return pack_transport;
    }

    public String getPack_activity() {
        return pack_activity;
    }

    public Blog getBlog() {
        return blog;
    }

    public List<Booking> getBookings() {
        return bookings;
    }



    //Setters//


    public void setId(Integer id) {
        this.id = id;
    }

    public void setPack_name(String pack_name) {
        this.pack_name = pack_name;
    }

    public void setPack_description(String pack_description) {
        this.pack_description = pack_description;
    }

    public void setPack_photo(String pack_photo) {
        this.pack_photo = pack_photo;
    }

    public void setPack_disponibility(Boolean pack_disponibility) {
        this.pack_disponibility = pack_disponibility;
    }

    public void setPack_depart_date(LocalDate pack_depart_date) {
        this.pack_depart_date = pack_depart_date;
    }

    public void setPack_return_date(LocalDate pack_return_date) {
        this.pack_return_date = pack_return_date;
    }

    public void setPack_quantity(Integer pack_quantity) {
        this.pack_quantity = pack_quantity;
    }

    public void setPack_price(Integer pack_price) {
        this.pack_price = pack_price;
    }

    public void setPack_accomodation(String pack_accomodation) {
        this.pack_accomodation = pack_accomodation;
    }

    public void setPack_transport(String pack_transport) {
        this.pack_transport = pack_transport;
    }

    public void setPack_activity(String pack_activity) {
        this.pack_activity = pack_activity;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}