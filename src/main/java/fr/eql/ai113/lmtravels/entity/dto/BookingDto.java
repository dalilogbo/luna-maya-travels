package fr.eql.ai113.lmtravels.entity.dto;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Activity;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.Payment;
import fr.eql.ai113.lmtravels.entity.Transport;
import fr.eql.ai113.lmtravels.entity.User;

import java.util.Date;

public class BookingDto {

    public Integer id;
    public Date booking_date;
    public Boolean insurance;
    public String status;
    private Payment payment;
    private User user;
    private Activity activity;
    private Pack aPack;
    private Transport transport;
    private Accommodation accommodation;

    ///Getters ///
    public Integer getId() {
        return id;
    }
    public Date getBooking_date() {
        return booking_date;
    }
    public Boolean getInsurance() {
        return insurance;
    }
    public String getStatus() {
        return status;
    }
    public Payment getPayment() {
        return payment;
    }
    public User getUser() {
        return user;
    }
    public Activity getActivity() {
        return activity;
    }
    public Pack getaPack() {
        return aPack;
    }
//    public Pack getaPack(Pack selectedPack) {
//        return aPack;
//    }
    public Transport getTransport() {
        return transport;
    }
    public Accommodation getAccommodation() {
        return accommodation;
    }

    ///Setter///


    public void setBooking_date(Date booking_date) {
        this.booking_date = booking_date;
    }

    ///Setters for Test ///

    public void setId(Integer id) {
        this.id = id;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setaPack(Pack aPack) {
        this.aPack = aPack;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }
}
