package fr.eql.ai113.lmtravels.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Payment {

    /**
     * Default constructor
     */
    public Payment() {
    }

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public Integer amount;
    public String mode;
    public String payment_date;

    /**
     * Association with Booking table
     */
    @OneToOne(targetEntity = Booking.class, mappedBy = "payment")
    private Booking booking;

    /**
     * Complete Payment constructor
     */
    public Payment(Integer id, Integer amount, String mode, String payment_date, Booking booking) {
        this.id = id;
        this.amount = amount;
        this.mode = mode;
        this.payment_date = payment_date;
        this.booking = booking;
    }

    /**
     * This method allows to Create a Payment
     */
    public void addPayment() {
        // TODO implement here
    }

    /**
     * This method allows to Search a Payment
     */
    public void searchPayment() {
        // TODO implement here
    }

    //Getters//

    public Integer getId() {
        return id;
    }

    public Integer getAmount() {
        return amount;
    }

    public String getMode() {
        return mode;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public Booking getBooking() {
        return booking;
    }


    //Setters//


    public void setId(Integer id) {
        this.id = id;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setPayment_date(String payment_date) {
        this.payment_date = payment_date;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
}