package fr.eql.ai113.lmtravels.controller.rest;

import fr.eql.ai113.lmtravels.entity.Booking;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.BookingDto;
import fr.eql.ai113.lmtravels.repository.BookingDao;
import fr.eql.ai113.lmtravels.repository.PackDao;
import fr.eql.ai113.lmtravels.repository.UserDao;
import fr.eql.ai113.lmtravels.service.BookingService;
import fr.eql.ai113.lmtravels.service.impl.BookingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("booking")
@CrossOrigin(origins = "${front.url}")
public class BookingRestController {

    private BookingService bookingService;
    @Autowired
    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    private UserDao userDao;
    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    private PackDao packDao;
    @Autowired
    public void setPackDao(PackDao packDao) {
        this.packDao = packDao;
    }

    private BookingDao bookingDao;
    @Autowired
    public void setBookingDao(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

//    @PostMapping("/bookPack")
//    public Booking bookPack(@RequestParam Integer user_id, @RequestParam Integer a_pack_id) {
//        User user= userDao.findById(user_id).orElseThrow(() -> new IllegalArgumentException("Invalid user ID"));
//        Pack pack = packDao.findById(a_pack_id).orElseThrow(() -> new IllegalArgumentException("Invalid pack ID"));
//
//        return bookingService.bookPack(user,pack);
//    }




}
