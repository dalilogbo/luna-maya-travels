package fr.eql.ai113.lmtravels.controller.rest;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Booking;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.dto.BookingDto;
import fr.eql.ai113.lmtravels.repository.AccommodationDao;
import fr.eql.ai113.lmtravels.repository.BookingDao;
import fr.eql.ai113.lmtravels.service.BookingService;
import fr.eql.ai113.lmtravels.service.PacksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("packs")
@CrossOrigin(origins = "${front.url}")
public class PacksRestController {

    PacksService packsService;
    @Autowired
    public void setPacksService(PacksService packsService) {
        this.packsService = packsService;
    }

    BookingService bookingService;
    @Autowired
    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    private BookingDao bookingDao;
    @Autowired
    public void setBookingDao(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }


    @GetMapping(path = "/findAllPacks")
    public @ResponseBody List<Pack> findAllPacks() {
        return packsService.findAllPacks();
    }

    @GetMapping("/findPackById/{id}")
    public Pack getPack(@PathVariable("id") Integer id) {
       return packsService.getPackById(id);
    }

    @PostMapping("/addBooking")
    public Booking saveBooking(@RequestBody BookingDto bookingDto) {return bookingService.saveBooking(bookingDto);}

    @GetMapping("/user/{userId}")
    public List<Booking> getAllBookingsByUserId(@PathVariable Integer userId) {
        return bookingDao.findAllByUserId(userId);
    }

    @GetMapping("/user/{id}/bookings")
    public List<Booking> findUserBookings(@PathVariable Integer id) {return bookingService.findUserBookings(id);}

    @GetMapping(path = "/findAllAccommodations")
    public @ResponseBody List<Accommodation> findAllAccommodations(){return packsService.findAllAccommodations();}

    @PostMapping(path = "/accommodationBooking")
    public Booking accommodationBooking(@RequestBody BookingDto bookingDto) {return bookingService.accommodationBooking(bookingDto);}

    @GetMapping("/user/{id}/accommodations")
    public List<Booking> userAccommodationsBookings(@PathVariable Integer id) {return bookingService.userAccommodationsBookings(id);}

    @GetMapping("/findAllBookings")
    public @ResponseBody List<Booking> findAllBookings(){return bookingService.findAllBookings();}

    @DeleteMapping("/cancelBooking/{id}")
    public void deleteBooking(@PathVariable("id") Integer id) {bookingService.deleteBookingById(id);}


}
