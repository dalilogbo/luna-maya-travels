package fr.eql.ai113.lmtravels.controller.rest;

import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.UserDto;
import fr.eql.ai113.lmtravels.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("registration")
@CrossOrigin(origins = "${front.url}")
public class RegistrationRestController {

    /**
     * Registration Service Injected by setter
     */
    RegistrationService registrationService;
    @Autowired
    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @PostMapping("/createuser")
    public User saveUser(@RequestBody UserDto userDto) {return registrationService.saveUser(userDto);}

    @PostMapping("/createadmin")
    public User saveAdmin(@RequestBody UserDto userDto) {return registrationService.saveAdmin(userDto);}

    @GetMapping(path = "/findAllUsers")
    public @ResponseBody List<User> findAllUsers(){return registrationService.findAllUsers();}

    @DeleteMapping("/deleteUser/{id}")
    public void deleteUser(@PathVariable("id") Integer id) {registrationService.deleteUserById(id);}


}
