package fr.eql.ai113.lmtravels.controller.rest;

import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("login")
@CrossOrigin(origins = "${front.url}")
public class LoginRestController {

    LoginService loginService;

    @GetMapping("/{login}/{password}")
    public User userConnection(@PathVariable String login, @PathVariable String password) {
        return loginService.authenticate(login, password);
    }

    @GetMapping("/users/{id}")
    public User retrieveUserById(@PathVariable int id) {
        return loginService.findUserById(id);
    }

    ///Setters///
    @Autowired
    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }
}
