package fr.eql.ai113.lmtravels.controller.rest;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.dto.AccommodationDto;
import fr.eql.ai113.lmtravels.entity.dto.PackDto;
import fr.eql.ai113.lmtravels.service.NewPackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("newPacks")
@CrossOrigin(origins = "${front.url}")
public class NewPacksRestController {

    NewPackService newPackService;

    @Autowired
    public void setNewPackService(NewPackService newPackService) {
        this.newPackService = newPackService;
    }

    @PostMapping("/createpack")
    public Pack savePack(@RequestBody PackDto packDto) {
        return newPackService.savePack(packDto);
    }

    @GetMapping(path = "/findAllPacks")
    public @ResponseBody List<Pack> findAllPacks() {
        return newPackService.findAllPacks();
    }

    @PostMapping("update")
    public void updatePack(@RequestBody Pack pack) {
        newPackService.updatePack(pack);
    }

    @DeleteMapping("/deletePack/{id}")
    public void deletePack(@PathVariable("id") Integer id) {
        newPackService.deletePackById(id);
    }

    @PostMapping("/createAccommodation")
    public Accommodation saveAccommodation(@RequestBody AccommodationDto accommodationDto) {return newPackService.saveAccommodation(accommodationDto);}

    @PostMapping("updateAccommodation")
    public void updateAccommodation(@RequestBody Accommodation accommodation) {newPackService.updateAccommodation(accommodation);}

    @DeleteMapping("/deleteAccommodation/{id}")
    public void deleteAccommodation(@PathVariable("id") Integer id) {newPackService.deleteAccommodationById(id);}


}
