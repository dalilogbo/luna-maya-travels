package fr.eql.ai113.lmtravels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class LmTravelsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LmTravelsApplication.class, args);
    }


}
