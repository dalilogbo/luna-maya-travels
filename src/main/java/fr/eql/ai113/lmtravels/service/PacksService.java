package fr.eql.ai113.lmtravels.service;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Booking;
import fr.eql.ai113.lmtravels.entity.Pack;

import java.util.List;

public interface PacksService {

    List<Pack> findAllPacksById(Integer id);

    List<Pack> findAllPacks();

    Pack getPackById(Integer id);

    abstract List<Accommodation> findAllAccommodations();

}

