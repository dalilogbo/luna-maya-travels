package fr.eql.ai113.lmtravels.service.impl;

import fr.eql.ai113.lmtravels.entity.Role;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.UserDto;
import fr.eql.ai113.lmtravels.repository.RoleDao;
import fr.eql.ai113.lmtravels.repository.UserDao;
import fr.eql.ai113.lmtravels.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private UserDao userDao;
    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    private RoleDao roleDao;
    @Autowired
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    private PasswordEncoder passwordEncoder;
    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Method to Create a new User with Role Client.
     * @param userDto
     * @return
     */
    @Override
    public User saveUser(UserDto userDto) {
        Role clientRole = roleDao.findById(1).orElseThrow(() -> new IllegalStateException("CLIENT role not found in database"));
        User user = new User(
                Arrays.asList(clientRole),
                userDto.getName(),
                userDto.getSurname(),
                userDto.getLogin(),
                passwordEncoder.encode(userDto.getPassword()),
                userDto.getPhone(),
                userDto.getAddress()
        );
        return userDao.save(user);
    }

    /**
     * Method to create a new User with Role Admin.
     * @param userDto
     * @return
     */
    @Override
    public User saveAdmin(UserDto userDto) {
        Role adminRole = roleDao.findById(2).orElseThrow(() -> new IllegalStateException("ADMIN role not found in database"));
        User user = new User(
                Arrays.asList(adminRole),
                userDto.getName(),
                userDto.getSurname(),
                userDto.getLogin(),
                passwordEncoder.encode(userDto.getPassword()),
                userDto.getPhone(),
                userDto.getAddress()
        );
        return userDao.save(user);
    }

    /**
     * Method to retrieve all Users.
     * @return
     */
    @Override
    public List<User> findAllUsers(){return userDao.findAll();}

    /**
     * Method to delete a User by Id.
     * @param id
     */
    @Override
    public void deleteUserById(Integer id) {userDao.deleteById(id);}


}


