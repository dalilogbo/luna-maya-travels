package fr.eql.ai113.lmtravels.service;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.dto.AccommodationDto;
import fr.eql.ai113.lmtravels.entity.dto.PackDto;

import java.util.List;

public interface NewPackService {

    Pack savePack(PackDto packDto);

    Accommodation saveAccommodation(AccommodationDto accommodationDto);

    List<Pack> findAllPacks();
    void updatePack(Pack pack);
    void deletePackById(Integer id);

    void deleteAccommodationById(Integer id);

    void updateAccommodation(Accommodation accommodation);
}
