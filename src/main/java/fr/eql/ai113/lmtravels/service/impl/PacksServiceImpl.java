package fr.eql.ai113.lmtravels.service.impl;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.repository.AccommodationDao;
import fr.eql.ai113.lmtravels.repository.BookingDao;
import fr.eql.ai113.lmtravels.repository.PackDao;
import fr.eql.ai113.lmtravels.service.PacksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacksServiceImpl implements PacksService {

    private PackDao packDao;
    @Autowired
    public void setPackDao(PackDao packDao) {
        this.packDao = packDao;
    }

    private AccommodationDao accommodationDao;
    @Autowired
    public void setAccommodationDao(AccommodationDao accommodationDao) {
        this.accommodationDao = accommodationDao;
    }

    private BookingDao bookingDao;
    @Autowired
    public void setBookingDao(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

    @Override
    public List<Pack> findAllPacksById(Integer id) {
        return packDao.findAllById(id);
    }

    @Override
    public List<Pack> findAllPacks(){
        return packDao.findAll();
    }

    @Override
    public Pack getPackById(Integer id) {
        return packDao.findById(id).get();
    }

    @Override
    public List<Accommodation> findAllAccommodations() {return accommodationDao.findAll();}

}
