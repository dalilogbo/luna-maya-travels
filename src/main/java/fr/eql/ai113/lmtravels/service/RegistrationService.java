package fr.eql.ai113.lmtravels.service;

import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.UserDto;

import java.util.List;

public interface RegistrationService {

    User saveUser(UserDto userDto);

    User saveAdmin(UserDto userDto);

    List<User> findAllUsers();

    void deleteUserById(Integer id);
}
