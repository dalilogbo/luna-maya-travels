package fr.eql.ai113.lmtravels.service.impl;

import fr.eql.ai113.lmtravels.entity.Booking;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.BookingDto;
import fr.eql.ai113.lmtravels.repository.BookingDao;
import fr.eql.ai113.lmtravels.repository.UserDao;
import fr.eql.ai113.lmtravels.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {

    private BookingDao bookingDao;
    @Autowired
    public void setBookingDao(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

    private UserDao userDao;
    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

//    /**
//     * Method to book a Pack.
//     * @param user
//     * @param pack
//     * @return Pack booked.
//     */
//    @Override
//    public Booking bookPack(User user, Pack pack) {
//        Booking booking = new Booking();
//        booking.setUser(user);
//        booking.setaPack(pack);
//
//        return bookingDao.save(booking);
//    }

    /**
     * Method to delete a Booking by ID.
     * @param id
     */
    @Override
    public void deleteBookingById(Integer id) {bookingDao.deleteById(id);}


    /**
     * Method to Book a Pack
     * @param bookingDto
     * @return Pack booked.
     */
    @Override
    public Booking saveBooking(BookingDto bookingDto) {
        Booking booking = new Booking(
                bookingDto.getUser(),
                bookingDto.getaPack()
        );
        return bookingDao.save(booking);
    }

    /**
     * Method to retrieve all Bookings from a User Id.
     * @param id
     * @return All user's bookings.
     */
    @Override
    public List<Booking> findUserBookings(Integer id) {return bookingDao.findAllByUserId(id);}

    /**
     * Method to Book an Accommodation.
     * @param bookingDto
     * @return Accommodation booked.
     */
    @Override
    public Booking accommodationBooking(BookingDto bookingDto) {
        Booking booking = new Booking(
                bookingDto.getUser(),
                bookingDto.getAccommodation()
        );
        booking.setBooking_date(bookingDto.getBooking_date()); // set booking date
        return bookingDao.save(booking);
    }

    /**
     * Method to Retrieve all Accommodation booking from a User Id
     * @param id
     * @return User's accommodations.
     */
    @Override
    public List<Booking> userAccommodationsBookings(Integer id) {return bookingDao.findAllByUserId(id);}

    /**
     * Method to Retrieve all Bookings.
     * @return all bookings.
     */
    @Override
    public List<Booking> findAllBookings() {return bookingDao.findAll();}

}