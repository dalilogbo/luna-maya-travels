package fr.eql.ai113.lmtravels.service.impl;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.dto.AccommodationDto;
import fr.eql.ai113.lmtravels.entity.dto.PackDto;
import fr.eql.ai113.lmtravels.repository.AccommodationDao;
import fr.eql.ai113.lmtravels.repository.PackDao;
import fr.eql.ai113.lmtravels.service.NewPackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewPackServiceImpl implements NewPackService {

    private PackDao packDao;
    @Autowired
    public void setPackDao(PackDao packDao) {
        this.packDao = packDao;
    }

    /**
     * Method to create a new Pack
     * @param packDto
     * @return pack
     */
    @Override
    public Pack savePack(PackDto packDto) {

        Pack pack = new Pack(
                packDto.getPack_name(),
                packDto.getPack_description(),
                packDto.getPack_photo(),
                packDto.getPack_disponibility(),
                packDto.getPack_depart_date(),
                packDto.getPack_return_date(),
                packDto.getPack_price(),
                packDto.getPack_accomodation(),
                packDto.getPack_transport(),
                packDto.getPack_activity()
        );
        return packDao.save(pack);
    }


    private AccommodationDao accommodationDao;
    @Autowired
    public void setAccommodationDao(AccommodationDao accommodationDao) {
        this.accommodationDao = accommodationDao;
    }

    /**
     * Method to create a new Accommodation.
     * @param accommodationDto
     * @return accommodation
     */
    @Override
    public Accommodation saveAccommodation(AccommodationDto accommodationDto) {

        Accommodation accommodation = new Accommodation(
                accommodationDto.getAccom_type(),
                accommodationDto.getAccom_name(),
                accommodationDto.getAccom_country(),
                accommodationDto.getAccom_city(),
                accommodationDto.getAccom_description(),
                accommodationDto.getAccom_room_type(),
                accommodationDto.getAccom_photo(),
                accommodationDto.getAccom_disponibility(),
                accommodationDto.getAccom_price()
        );
        return accommodationDao.save(accommodation);
    }

    /**
     * Method to Retrieve all Packs.
     * @return all Packs
     */
    @Override
    public List<Pack> findAllPacks(){
        return packDao.findAll();
    }

    /**
     * Method to update a Pack information.
     * @param pack
     */
    @Override
    public void updatePack(Pack pack) {
        packDao.save(pack);
    }

    /**
     * Method to delete a Pack by Id.
     * @param id
     */
    @Override
    public void deletePackById(Integer id) {
        packDao.deleteById(id);
    }

    /**
     * Method to delete an Accommodation by Id.
     * @param id
     */
    @Override
    public void deleteAccommodationById(Integer id) {accommodationDao.deleteById(id);}

    /**
     * Method to update Accommodation information.
     * @param accommodation
     */
    @Override
    public void updateAccommodation(Accommodation accommodation) {accommodationDao.save(accommodation);}


}
