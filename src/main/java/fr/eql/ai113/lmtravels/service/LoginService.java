package fr.eql.ai113.lmtravels.service;


import fr.eql.ai113.lmtravels.entity.User;

public interface LoginService {

    User authenticate(String user_name, String password);
    User findUserById(int id);

}
