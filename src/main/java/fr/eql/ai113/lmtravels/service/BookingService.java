package fr.eql.ai113.lmtravels.service;

import fr.eql.ai113.lmtravels.entity.Booking;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.BookingDto;

import java.util.List;

public interface BookingService {

//    Booking bookPack(User user, Pack pack);

    void deleteBookingById(Integer id);

    Booking saveBooking(BookingDto bookingDto);

    List<Booking> findUserBookings(Integer id);

    Booking accommodationBooking(BookingDto bookingDto);

    List<Booking> userAccommodationsBookings(Integer id);

    List<Booking> findAllBookings();
}
