package fr.eql.ai113.lmtravels.service.impl;

import fr.eql.ai113.lmtravels.repository.UserDao;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {


    private UserDao userDao;

    @Override
    public User authenticate(String login, String password) {
        return userDao.findByLoginAndPassword(login, password);
    }

    @Override
    public User findUserById(int id) {
        return userDao.findById(id);
    }


    // Setters //

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
