package fr.eql.ai113.lmtravels.exception;

public class UserInexistantException extends Exception {

    public UserInexistantException (String s) {
        super(s);
    }

}
