package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer> {

}
