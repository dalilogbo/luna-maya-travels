package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityDao extends JpaRepository<Activity, Integer> {


}
