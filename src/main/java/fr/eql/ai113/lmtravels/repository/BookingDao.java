package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

public interface BookingDao extends JpaRepository<Booking,Integer> {

    List<Booking> findAllByUserId(Integer id);


}
