package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentDao extends JpaRepository<Payment,Integer> {


}
