package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Pack;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccommodationDao extends JpaRepository<Accommodation, Integer> {

    List<Accommodation> findAllById(Integer id);

}
