package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

    User findByLoginAndPassword(String login, String password);
    List<User> findAllByIdNot(Integer id);
//    Optional<User> findById(Integer id);
    User findById(int id);
    User findByNameAndSurname(String name, String surname);
    User findByLogin(String login);

}
