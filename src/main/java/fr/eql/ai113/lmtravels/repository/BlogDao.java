package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Blog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlogDao extends JpaRepository<Blog,Integer> {


}
