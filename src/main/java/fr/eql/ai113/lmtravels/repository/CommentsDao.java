package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentsDao extends JpaRepository<Comments,Integer> {


}
