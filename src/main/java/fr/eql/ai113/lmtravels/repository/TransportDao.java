package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Transport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransportDao extends JpaRepository<Transport,Integer> {


}
