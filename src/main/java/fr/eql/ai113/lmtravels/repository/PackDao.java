package fr.eql.ai113.lmtravels.repository;

import fr.eql.ai113.lmtravels.entity.Pack;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PackDao extends JpaRepository<Pack,Integer> {


    List<Pack> findAllById(Integer id);


}
