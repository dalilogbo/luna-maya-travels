package fr.eql.ai113.lmtravels;

import fr.eql.ai113.lmtravels.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class UserServiceImplTest {


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void testGenerateJwtForUser() {
        String username = "testuser";

        UserDetails userDetails = mock(UserDetails.class);
        when(userDetails.getUsername()).thenReturn(username);

        String signingKey = "testsigningkey";
        UserServiceImpl userService = new UserServiceImpl(signingKey);

        String jwt = userService.generateJwtForUser(userDetails);

        assertNotNull(jwt);
        verify(userDetails, times(1)).getUsername();
    }
}
