package fr.eql.ai113.lmtravels;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.repository.AccommodationDao;
import fr.eql.ai113.lmtravels.repository.BookingDao;
import fr.eql.ai113.lmtravels.repository.PackDao;
import fr.eql.ai113.lmtravels.service.impl.PacksServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class PacksServiceImplTest {

    @InjectMocks
    private PacksServiceImpl packsService;

    @Mock
    private PackDao packDao;

    @Mock
    private AccommodationDao accommodationDao;

    @Mock
    private BookingDao bookingDao;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindAllPacksById() {
        // Prepare mock data
        Integer id = 1;
        List<Pack> expectedPacks = new ArrayList<>();
        LocalDate departDate = LocalDate.now();
        LocalDate returnDate = departDate.plusDays(7);
        expectedPacks.add(new Pack(1, "Pack 1", "Description 1", "photo1.jpg", true, departDate, returnDate, 10, 100, "Accommodation 1", "Transport 1", "Activity 1"));
        expectedPacks.add(new Pack(2, "Pack 2", "Description 2", "photo2.jpg", true, departDate, returnDate, 20, 200, "Accommodation 2", "Transport 2", "Activity 2"));
        when(packDao.findAllById(id)).thenReturn(expectedPacks);

        // Call the service method
        List<Pack> actualPacks = packsService.findAllPacksById(id);

        // Assert the result
        assertEquals(expectedPacks, actualPacks);
        verify(packDao, times(1)).findAllById(id);
    }

    @Test
    public void testFindAllPacks() {
        // Prepare mock data
        Pack pack1 = new Pack();
        pack1.setId(1);
        Pack pack2 = new Pack();
        pack2.setId(2);
        List<Pack> packList = new ArrayList<>();
        packList.add(pack1);
        packList.add(pack2);

        when(packDao.findAll()).thenReturn(packList);

        // Call the service method
        List<Pack> result = packsService.findAllPacks();

        // Assert the result
        assertEquals(2, result.size());
        assertEquals(1, result.get(0).getId());
        assertEquals(2, result.get(1).getId());

        // Verify that the packDao.findAll() method was called
        verify(packDao, times(1)).findAll();
    }

    @Test
    public void testGetPackById() {
        // Prepare mock data
        Pack pack = new Pack();
        pack.setId(1);
        Optional<Pack> optionalPack = Optional.of(pack);

        when(packDao.findById(1)).thenReturn(optionalPack);

        // Call the service method
        Pack result = packsService.getPackById(1);

        // Assert the result
        assertEquals(1, result.getId());

        // Verify that the packDao.findById() method was called
        verify(packDao, times(1)).findById(1);
    }

    @Test
    public void testFindAllAccommodations() {
        // Prepare mock data
        Accommodation accommodation1 = new Accommodation();
        accommodation1.setId(1);
        Accommodation accommodation2 = new Accommodation();
        accommodation2.setId(2);
        List<Accommodation> accommodationList = new ArrayList<>();
        accommodationList.add(accommodation1);
        accommodationList.add(accommodation2);

        when(accommodationDao.findAll()).thenReturn(accommodationList);

        // Call the service method
        List<Accommodation> result = packsService.findAllAccommodations();

        // Assert the result
        assertEquals(2, result.size());
        assertEquals(1, result.get(0).getId());
        assertEquals(2, result.get(1).getId());

        // Verify that the accommodationDao.findAll() method was called
        verify(accommodationDao, times(1)).findAll();
    }
}
