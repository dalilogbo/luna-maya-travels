package fr.eql.ai113.lmtravels;

import static org.mockito.Mockito.*;

import fr.eql.ai113.lmtravels.repository.UserDao;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.service.impl.LoginServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LoginServiceImplTest {

    @InjectMocks
    private LoginServiceImpl loginService;

    @Mock
    private UserDao userDao;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testAuthenticate() {
        // Mock data
        String login = "testuser";
        String password = "testpassword";
        User mockUser = new User();
        mockUser.setLogin(login);
        mockUser.setPassword(password);

        // Mock userDao's findByLoginAndPassword() method
        when(userDao.findByLoginAndPassword(login, password)).thenReturn(mockUser);

        // Call the authenticate() method
        User result = loginService.authenticate(login, password);

        // Verify that userDao's findByLoginAndPassword() method was called with correct arguments
        verify(userDao, times(1)).findByLoginAndPassword(login, password);

        // Assert that the returned user is not null and has correct login and password
        assertNotNull(result);
        assertEquals(login, result.getLogin());
        assertEquals(password, result.getPassword());
    }

    @Test
    void testFindUserById() {
        // Mock data
        int id = 1;
        User mockUser = new User();
        mockUser.setId(id);

        // Mock userDao's findById() method
        when(userDao.findById(id)).thenReturn(mockUser);

        // Call the findUserById() method
        User result = loginService.findUserById(id);

        // Verify that userDao's findById() method was called with correct argument
        verify(userDao, times(1)).findById(id);

        // Assert that the returned user is not null and has correct id
        assertNotNull(result);
        assertEquals(id, result.getId());
    }
}
