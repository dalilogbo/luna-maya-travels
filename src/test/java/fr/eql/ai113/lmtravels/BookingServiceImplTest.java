package fr.eql.ai113.lmtravels;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Booking;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.BookingDto;
import fr.eql.ai113.lmtravels.repository.BookingDao;
import fr.eql.ai113.lmtravels.service.impl.BookingServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.mockito.Mockito.*;

public class BookingServiceImplTest {
    @InjectMocks
    private BookingServiceImpl bookingService;

    @Mock
    private BookingDao bookingDao;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSaveBooking() {
        // Create a BookingDto object
        BookingDto bookingDto = new BookingDto();
        User user = new User();
        Pack aPack = new Pack();
        bookingDto.setUser(user);
        bookingDto.setaPack(aPack);

        // Create a Booking object
        Booking booking = new Booking(user, aPack);

        // Mock the behavior of the bookingDao.save() method
        Mockito.when(bookingDao.save(Mockito.any(Booking.class))).thenReturn(booking);

        // Call the saveBooking method and capture the returned Booking object
        Booking savedBooking = bookingService.saveBooking(bookingDto);

        // Verify that the bookingDao.save() method was called with the correct Booking object
        Mockito.verify(bookingDao).save(Mockito.any(Booking.class));

        // Assert that the returned Booking object is not null
        Assertions.assertNotNull(savedBooking);

        // Assert that the returned Booking object is equal to the expected Booking object
        Assertions.assertEquals(booking, savedBooking);
    }


    @Test
    public void testDeleteBookingById() {
        // Prepare test data
        Booking booking = new Booking();
        booking.setId(1);

        // Call the method under test
        bookingService.deleteBookingById(1);

        // Verify that the appropriate method was called
        verify(bookingDao, times(1)).deleteById(1);
    }

    @Test
    public void testFindUserBookings() {
        // Create a test user ID
        Integer userId = 123;

        // Create a list of Booking objects to be returned by the bookingDao.findAllByUserId() method
        List<Booking> bookings = new ArrayList<>();
        bookings.add(new Booking());
        bookings.add(new Booking());

        // Mock the behavior of the bookingDao.findAllByUserId() method
        Mockito.when(bookingDao.findAllByUserId(userId)).thenReturn(bookings);

        // Call the findUserBookings method and capture the returned list of Booking objects
        List<Booking> foundBookings = bookingService.findUserBookings(userId);

        // Verify that the bookingDao.findAllByUserId() method was called with the correct user ID
        Mockito.verify(bookingDao).findAllByUserId(userId);

        // Assert that the returned list of Booking objects is not null
        Assertions.assertNotNull(foundBookings);

        // Assert that the size of the returned list of Booking objects is equal to the size of the expected list
        Assertions.assertEquals(bookings.size(), foundBookings.size());

        // Assert that the contents of the returned list of Booking objects are equal to the expected list
        Assertions.assertEquals(bookings, foundBookings);
    }

    @Test
    public void testAccommodationBooking() {
        // Create a test BookingDto object
        BookingDto bookingDto = new BookingDto();
        bookingDto.setUser(new User());
        bookingDto.setAccommodation(new Accommodation());
        bookingDto.setBooking_date(new Date()); // Set the booking date to the current date

        // Mock the behavior of the bookingDao.save() method
        Booking savedBooking = new Booking();
        Mockito.when(bookingDao.save(Mockito.any(Booking.class))).thenReturn(savedBooking);

        // Call the accommodationBooking method and capture the returned Booking object
        Booking result = bookingService.accommodationBooking(bookingDto);

        // Verify that the bookingDao.save() method was called with the correct Booking object
        Mockito.verify(bookingDao).save(Mockito.any(Booking.class));

        // Assert that the returned Booking object is not null
        Assertions.assertNotNull(result);

        // Assert that the returned Booking object is the same as the savedBooking object
        Assertions.assertSame(savedBooking, result);
    }

    @Test
    public void testUserAccommodationsBookings() {
        // Create a test user ID
        Integer userId = 1;

        // Create a list of test Booking objects
        List<Booking> bookings = new ArrayList<>();
        bookings.add(new Booking());
        bookings.add(new Booking());
        bookings.add(new Booking());

        // Mock the behavior of the bookingDao.findAllByUserId() method
        Mockito.when(bookingDao.findAllByUserId(userId)).thenReturn(bookings);

        // Call the userAccommodationsBookings method and capture the returned list of Booking objects
        List<Booking> result = bookingService.userAccommodationsBookings(userId);

        // Verify that the bookingDao.findAllByUserId() method was called with the correct user ID
        Mockito.verify(bookingDao).findAllByUserId(userId);

        // Assert that the returned list of Booking objects is not null
        Assertions.assertNotNull(result);

        // Assert that the returned list of Booking objects is the same as the bookings list
        Assertions.assertSame(bookings, result);
    }

    @Test
    public void testFindAllBookings() {
        // Create a list of test Booking objects
        List<Booking> bookings = new ArrayList<>();
        bookings.add(new Booking());
        bookings.add(new Booking());
        bookings.add(new Booking());

        // Mock the behavior of the bookingDao.findAll() method
        Mockito.when(bookingDao.findAll()).thenReturn(bookings);

        // Call the findAllBookings method and capture the returned list of Booking objects
        List<Booking> result = bookingService.findAllBookings();

        // Verify that the bookingDao.findAll() method was called
        Mockito.verify(bookingDao).findAll();

        // Assert that the returned list of Booking objects is not null
        Assertions.assertNotNull(result);

        // Assert that the returned list of Booking objects is the same as the bookings list
        Assertions.assertSame(bookings, result);
    }


}
