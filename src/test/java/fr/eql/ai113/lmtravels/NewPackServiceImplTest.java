package fr.eql.ai113.lmtravels;

import fr.eql.ai113.lmtravels.entity.Accommodation;
import fr.eql.ai113.lmtravels.entity.Pack;
import fr.eql.ai113.lmtravels.entity.dto.AccommodationDto;
import fr.eql.ai113.lmtravels.entity.dto.PackDto;
import fr.eql.ai113.lmtravels.repository.AccommodationDao;
import fr.eql.ai113.lmtravels.repository.PackDao;
import fr.eql.ai113.lmtravels.service.impl.NewPackServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class NewPackServiceImplTest {

    @InjectMocks
    private NewPackServiceImpl newPackService;

    @Mock
    private PackDao packDao;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSavePack() {
        // Input data
        PackDto packDto = new PackDto();
        packDto.setPack_name("name");
        packDto.setPack_description("description");
        packDto.setPack_photo("photo");
        packDto.setPack_disponibility(true);
        packDto.setPack_depart_date(LocalDate.of(2023, 4, 16));
        packDto.setPack_return_date(LocalDate.of(2023, 4, 20));
        packDto.setPack_price(200);
        packDto.setPack_accomodation("accommodation");
        packDto.setPack_transport("transport");
        packDto.setPack_activity("activity");

        // Expected output
        Pack pack = new Pack(
                "name",
                "description",
                "photo",
                true,
                LocalDate.of(2023, 4, 16),
                LocalDate.of(2023, 4, 20),
                200,
                "accommodation",
                "transport",
                "activity"
        );

        // Mock the behavior of the packDao.save() method
        when(packDao.save(any(Pack.class))).thenReturn(pack);

        // Call the method to be tested
        Pack result = newPackService.savePack(packDto);

        // Assert the result
        assertNotNull(result);
        assertEquals(pack.getPack_name(), result.getPack_name());
        assertEquals(pack.getPack_description(), result.getPack_description());
        assertEquals(pack.getPack_photo(), result.getPack_photo());
        assertEquals(pack.getPack_disponibility(), result.getPack_disponibility());
        assertEquals(pack.getPack_depart_date(), result.getPack_depart_date());
        assertEquals(pack.getPack_return_date(), result.getPack_return_date());
        assertEquals(pack.getPack_price(), result.getPack_price(), 0);
        assertEquals(pack.getPack_accomodation(), result.getPack_accomodation());
        assertEquals(pack.getPack_transport(), result.getPack_transport());
        assertEquals(pack.getPack_activity(), result.getPack_activity());

        // Verify that packDao.save() was called exactly once with the correct argument
        verify(packDao, times(1)).save(any(Pack.class));
    }


    @Mock
    private AccommodationDao accommodationDao;

    @Test
    public void testSaveAccommodation() {
        // Input data
        AccommodationDto accommodationDto = new AccommodationDto();
        accommodationDto.setAccom_type("type");
        accommodationDto.setAccom_name("name");
        accommodationDto.setAccom_country("country");
        accommodationDto.setAccom_city("city");
        accommodationDto.setAccom_description("description");
        accommodationDto.setAccom_room_type("room type");
        accommodationDto.setAccom_photo("photo");
        accommodationDto.setAccom_disponibility(true);
        accommodationDto.setAccom_price(100);
        // Expected output
        Accommodation accommodation = new Accommodation(
                "type",
                "name",
                "country",
                "city",
                "description",
                "room type",
                "photo",
                true,
                100
        );

        // Mock the behavior of the accommodationDao.save() method
        when(accommodationDao.save(any(Accommodation.class))).thenReturn(accommodation);

        // Call the method to be tested
        Accommodation result = newPackService.saveAccommodation(accommodationDto);

        // Assert the result
        assertNotNull(result);
        assertEquals(accommodation.getAccom_type(), result.getAccom_type());
        assertEquals(accommodation.getAccom_name(), result.getAccom_name());
        assertEquals(accommodation.getAccom_country(), result.getAccom_country());
        assertEquals(accommodation.getAccom_city(), result.getAccom_city());
        assertEquals(accommodation.getAccom_description(), result.getAccom_description());
        assertEquals(accommodation.getAccom_room_type(), result.getAccom_room_type());
        assertEquals(accommodation.getAccom_photo(), result.getAccom_photo());
        assertEquals(accommodation.getAccom_disponibility(), result.getAccom_disponibility());
        assertEquals(accommodation.getAccom_price(), result.getAccom_price(), 0);

        // Verify that accommodationDao.save() was called exactly once with the correct argument
        verify(accommodationDao, times(1)).save(any(Accommodation.class));
    }


    @Test
    public void testFindAllPacks() {
        // Mocking the PackDao's findAll() method
        List<Pack> expectedPacks = new ArrayList<>();
        expectedPacks.add(new Pack());
        expectedPacks.add(new Pack());
        when(packDao.findAll()).thenReturn(expectedPacks);

        // Calling the service method
        List<Pack> actualPacks = newPackService.findAllPacks();

        // Asserting the result
        assertEquals(expectedPacks, actualPacks);
        verify(packDao, times(1)).findAll();
    }

    @Test
    public void testUpdatePack() {
        // Prepare test data
        Pack packToUpdate = new Pack();
        packToUpdate.setId(1);
        packToUpdate.setPack_name("Test Pack Name");
        packToUpdate.setPack_description("Test Pack Description");
        packToUpdate.setPack_photo("Test Pack Photo");
        packToUpdate.setPack_disponibility(true);
        packToUpdate.setPack_depart_date(LocalDate.now());
        packToUpdate.setPack_return_date(LocalDate.now());
        packToUpdate.setPack_price(100);
        packToUpdate.setPack_accomodation("Test Accommodation");
        packToUpdate.setPack_transport("Test Transport");
        packToUpdate.setPack_activity("Test activity");

        // Calling the service method
        newPackService.updatePack(packToUpdate);

        // Verifying that packDao.save() was called with the correct argument
        verify(packDao, times(1)).save(eq(packToUpdate));
    }

    @Test
    public void testDeletePackById() {
        // Prepare test data
        Integer packId = 1;

        // Calling the service method
        newPackService.deletePackById(packId);

        // Verifying that packDao.deleteById() was called with the correct argument
        verify(packDao, times(1)).deleteById(eq(packId));
    }

    @Test
    public void testDeleteAccommodationById() {
        // Prepare test data
        Integer accommodationId = 1;

        // Calling the service method
        newPackService.deleteAccommodationById(accommodationId);

        // Verifying that accommodationDao.deleteById() was called with the correct argument
        verify(accommodationDao, times(1)).deleteById(eq(accommodationId));
    }

    @Test
    public void testUpdateAccommodation() {
        // Prepare test data
        Accommodation accommodationToUpdate = new Accommodation();
        accommodationToUpdate.setId(1);
        accommodationToUpdate.setAccom_type("Test Type");
        accommodationToUpdate.setAccom_name("Test Name");
        accommodationToUpdate.setAccom_country("Test Country");
        accommodationToUpdate.setAccom_city("Test City");
        accommodationToUpdate.setAccom_description("Test Description");
        accommodationToUpdate.setAccom_room_type("Test Room Type");
        accommodationToUpdate.setAccom_photo("Test Photo");
        accommodationToUpdate.setAccom_disponibility(true);
        accommodationToUpdate.setAccom_price(100);

        // Calling the service method
        newPackService.updateAccommodation(accommodationToUpdate);

        // Verifying that accommodationDao.save() was called with the correct argument
        verify(accommodationDao, times(1)).save(eq(accommodationToUpdate));
    }


}
