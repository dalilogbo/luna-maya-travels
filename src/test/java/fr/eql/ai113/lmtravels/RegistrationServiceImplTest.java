package fr.eql.ai113.lmtravels;

import fr.eql.ai113.lmtravels.entity.Role;
import fr.eql.ai113.lmtravels.entity.User;
import fr.eql.ai113.lmtravels.entity.dto.UserDto;
import fr.eql.ai113.lmtravels.repository.RoleDao;
import fr.eql.ai113.lmtravels.repository.UserDao;
import fr.eql.ai113.lmtravels.service.impl.RegistrationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RegistrationServiceImplTest {

    @InjectMocks
    private RegistrationServiceImpl registrationService;

    @Mock
    private UserDao userDao;

    @Mock
    private RoleDao roleDao;

    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSaveUser() {
        // Prepare test data
        UserDto userDto = new UserDto();
        userDto.setName("John");
        userDto.setSurname("Doe");
        userDto.setLogin("johndoe");
        userDto.setPassword("password");
        userDto.setPhone("1234567890");
        userDto.setAddress("1234 Elm St");

        Role clientRole = new Role();
        clientRole.setId(1);
        clientRole.setName("CLIENT");

        when(roleDao.findById(1)).thenReturn(Optional.of(clientRole));
        when(passwordEncoder.encode("password")).thenReturn("encodedPassword");

        User expectedUser = new User();
        expectedUser.setId(1);
        expectedUser.setRoles(Arrays.asList(clientRole));
        expectedUser.setName("John");
        expectedUser.setSurname("Doe");
        expectedUser.setLogin("johndoe");
        expectedUser.setPassword("encodedPassword");
        expectedUser.setPhone("1234567890");
        expectedUser.setAddress("1234 Elm St");

        when(userDao.save(any(User.class))).thenReturn(expectedUser);

        // Call the method being tested
        User actualUser = registrationService.saveUser(userDto);

        // Assert the result
        assertNotNull(actualUser);
        assertEquals(1, actualUser.getId());
        assertEquals("John", actualUser.getName());
        assertEquals("Doe", actualUser.getSurname());
        assertEquals("johndoe", actualUser.getLogin());
        assertEquals("encodedPassword", actualUser.getPassword());
        assertEquals("1234567890", actualUser.getPhone());
        assertEquals("1234 Elm St", actualUser.getAddress());

        verify(roleDao, times(1)).findById(1);
        verify(passwordEncoder, times(1)).encode("password");
        verify(userDao, times(1)).save(any(User.class));
    }


    @Test
    public void testSaveAdmin() {
        // Prepare test data
        UserDto userDto = new UserDto();
        userDto.setName("John");
        userDto.setSurname("Doe");
        userDto.setLogin("johndoe");
        userDto.setPassword("password");
        userDto.setPhone("1234567890");
        userDto.setAddress("1234 Elm St");

        Role adminRole = new Role();
        adminRole.setId(2);
        adminRole.setName("ADMIN");

        when(roleDao.findById(2)).thenReturn(Optional.of(adminRole));
        when(passwordEncoder.encode("password")).thenReturn("encodedPassword");

        User expectedUser = new User();
        expectedUser.setId(1);
        expectedUser.setRoles(Arrays.asList(adminRole));
        expectedUser.setName("John");
        expectedUser.setSurname("Doe");
        expectedUser.setLogin("johndoe");
        expectedUser.setPassword("encodedPassword");
        expectedUser.setPhone("1234567890");
        expectedUser.setAddress("1234 Elm St");

        when(userDao.save(any(User.class))).thenReturn(expectedUser);

        // Call the method being tested
        User actualUser = registrationService.saveAdmin(userDto);

        // Assert the result
        assertNotNull(actualUser);
        assertEquals(1, actualUser.getId());
        assertEquals("John", actualUser.getName());
        assertEquals("Doe", actualUser.getSurname());
        assertEquals("johndoe", actualUser.getLogin());
        assertEquals("encodedPassword", actualUser.getPassword());
        assertEquals("1234567890", actualUser.getPhone());
        assertEquals("1234 Elm St", actualUser.getAddress());

        verify(roleDao, times(1)).findById(2);
        verify(passwordEncoder, times(1)).encode("password");
        verify(userDao, times(1)).save(any(User.class));
    }

    @Test
    public void testFindAllUsers() {
        // Prepare test data
        Role clientRole = new Role();
        clientRole.setId(1);
        clientRole.setName("CLIENT");

        Role adminRole = new Role();
        adminRole.setId(2);
        adminRole.setName("ADMIN");

        User user1 = new User();
        user1.setId(1);
        user1.setRoles(Arrays.asList(clientRole));
        user1.setName("John");
        user1.setSurname("Doe");
        user1.setLogin("johndoe");
        user1.setPassword("encodedPassword1");
        user1.setPhone("1234567890");
        user1.setAddress("1234 Elm St");

        User user2 = new User();
        user2.setId(2);
        user2.setRoles(Arrays.asList(adminRole));
        user2.setName("Jane");
        user2.setSurname("Doe");
        user2.setLogin("janedoe");
        user2.setPassword("encodedPassword2");
        user2.setPhone("9876543210");
        user2.setAddress("5678 Oak St");

        List<User> expectedUsers = Arrays.asList(user1, user2);

        when(userDao.findAll()).thenReturn(expectedUsers);

        // Call the method being tested
        List<User> actualUsers = registrationService.findAllUsers();

        // Assert the result
        assertEquals(expectedUsers.size(), actualUsers.size());
        for (int i = 0; i < expectedUsers.size(); i++) {
            User expectedUser = expectedUsers.get(i);
            User actualUser = actualUsers.get(i);
            assertEquals(expectedUser.getId(), actualUser.getId());
            assertEquals(expectedUser.getName(), actualUser.getName());
            assertEquals(expectedUser.getSurname(), actualUser.getSurname());
            assertEquals(expectedUser.getLogin(), actualUser.getLogin());
            assertEquals(expectedUser.getPassword(), actualUser.getPassword());
            assertEquals(expectedUser.getPhone(), actualUser.getPhone());
            assertEquals(expectedUser.getAddress(), actualUser.getAddress());
        }

        verify(userDao, times(1)).findAll();
    }

    @Test
    public void testDeleteUserById() {
        // Prepare test data
        Integer userId = 1;

        // Call the method being tested
        registrationService.deleteUserById(userId);

        // Verify that userDao.deleteById() was called with the correct userId
        verify(userDao, times(1)).deleteById(userId);
    }


}

